﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUIHub : MonoBehaviour
{
    public PlayerController playerController;

    private void OnEnable()
    {
        playerController.enabled = true;
    }

    private void OnDisable()
    {
        playerController.enabled = false;
    }
}
