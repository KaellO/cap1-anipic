﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PlayerAction : MonoBehaviour
{
    [Header("Croc")]
    public GameObject CrocPanel;
    public FadeTransitionUI FadeTransition;
    public GameObject respawn;
    public MainGameUI maingameUI;
    public GameObject MainGameUIGameObject;
    float playerSpeed = 0;
    Movement movement;
    PlayerController cc;
    CharacterController characterController;

    bool isFading = false;
    private void Awake()
    {
        movement = GetComponent<Movement>();
        playerSpeed = movement.speed;
        cc = GetComponent<PlayerController>();
        characterController = GetComponent<CharacterController>();

    }
    public void Crocodile()
    {
        if (!isFading)
        {
            characterController.enabled = false;
            isFading = true;
            GetComponent<CameraMechanic>().DisableUI();
            cc.enabled = false;
            FadeTransition.BlockRayCast(true);
            GetComponent<Footsteps>().enabled = false;
            FadeTransition.Fade(1f, 2f, Teleport);
        }
    }

    public void Teleport()
    {
        Debug.Log("teleport to " + respawn.transform.position);
        transform.position = respawn.transform.position;
        GetComponent<CameraMechanic>().DisableUI();
        maingameUI.ActivatePanel(MainGameUIGameObject);
        characterController.enabled = true;
        GetComponent<Movement>().speed = 5;
        FadeTransition.Fade(0f, 1f, () =>
        {
            CrocPanel.SetActive(true);
            FadeTransition.BlockRayCast(false);
            isFading = false;
            GetComponent<Footsteps>().enabled = true;
            GetComponent<Footsteps>().ChangeFootstep("grass");
        });
    }
}
