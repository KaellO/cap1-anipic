﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CompendiumUI : MonoBehaviour
{
    [SerializeField] GameObject Tamaraw;
    [SerializeField] GameObject Turtle;
    [SerializeField] GameObject Croc;
    [SerializeField] GameObject MouseDeer;
    
    [SerializeField] QuestManager qManager;
    [SerializeField] GameObject Alert;

    public void Init()
    {
        qManager.OnThreeStarPhoto += UnlockCompendium;

        //Alert.SetActive(true);
        SetThumbnailUnlock();
    }

    private void OnEnable()
    {
        Alert.SetActive(false);
    }

    void UnlockCompendium(Photo photo)
    {
        switch (photo.Subject)
        {
            case "Tamaraw":
                UnlockThumbnail(Tamaraw, photo.Subject);
                TempSavedData.UnlockedTamaraw = true;
                break;
            case "Sea Turtle":
                UnlockThumbnail(Turtle, photo.Subject);
                TempSavedData.UnlockedTurtle = true;
                break;
            case "Crocodile":
                UnlockThumbnail(Croc, photo.Subject);
                TempSavedData.UnlockedCroc = true;
                break;
            case "Mouse Deer":
                UnlockThumbnail(MouseDeer, photo.Subject);
                TempSavedData.UnlockedMouseDeer = true;
                break;
        }
    }

    void UnlockThumbnail(GameObject thumbnail, string name)
    {
        CompendiumThumbUI thumbUI = thumbnail.GetComponent<CompendiumThumbUI>();
        if (!thumbUI.Unlocked)
        {
            Alert.SetActive(true);
        }
        thumbUI.Unlock();
        thumbnail.GetComponent<CompendiumThumbUI>().SubjectName.GetComponent<TextMeshProUGUI>().text = name;    
    }

    void SetThumbnailUnlock()
    {
        if(TempSavedData.UnlockedTamaraw)
        {
            Tamaraw.GetComponent<CompendiumThumbUI>().Unlock();
            Tamaraw.GetComponent<CompendiumThumbUI>().SubjectName.GetComponent<TextMeshProUGUI>().text = "Tamaraw";
        }

        if (TempSavedData.UnlockedTurtle)
        {
            Turtle.GetComponent<CompendiumThumbUI>().Unlock();
            Turtle.GetComponent<CompendiumThumbUI>().SubjectName.GetComponent<TextMeshProUGUI>().text = "Sea Turtle";
        }

        if (TempSavedData.UnlockedCroc)
        {
            Croc.GetComponent<CompendiumThumbUI>().Unlock();
            Croc.GetComponent<CompendiumThumbUI>().SubjectName.GetComponent<TextMeshProUGUI>().text = "Crocodile";
        }

        if (TempSavedData.UnlockedMouseDeer)
        {
            MouseDeer.GetComponent<CompendiumThumbUI>().Unlock();
            MouseDeer.GetComponent<CompendiumThumbUI>().SubjectName.GetComponent<TextMeshProUGUI>().text = "Mouse Deer";
        }
    }
}
