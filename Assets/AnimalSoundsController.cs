﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSoundsController : MonoBehaviour
{
    public AudioClip[] AnimalSounds;
    public float MinRandomRange = 4f;
    public float MaxRandomRange = 8f;

    AudioSource audioSource;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        if (AnimalSounds.Length > 0)
        {
            StartCoroutine(PlaySound());
        }
    }

    IEnumerator PlaySound()
    {
        while(this.gameObject)
        {
            float seconds = Random.Range(MinRandomRange, MaxRandomRange);
            yield return new WaitForSeconds(seconds);

            AudioClip randomSound = AnimalSounds[Random.Range(0, AnimalSounds.Length)];

            audioSource.clip = randomSound;
            audioSource.Play();
        }
    }
}
