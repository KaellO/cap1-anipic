﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MangroveArea : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<Movement>().speed -= 2;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Movement>().speed += 2;
        }
    }
}
