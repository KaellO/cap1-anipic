﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelViewerUI : MonoBehaviour
{
    public GameObject AnimalMesh;
    public GameObject AnimalRenderer;
    public GameObject rotator;

    Quaternion oldRot;
    private void OnEnable()
    {
        oldRot = rotator.transform.rotation;
    }
    public void OnExit()
    {
        AnimalMesh.SetActive(false);
        AnimalRenderer.SetActive(false);
        rotator.transform.rotation = oldRot;
    }
}
