﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraResolution : MonoBehaviour
{
    public bool UseCustomRes;
    void Awake()
    {
        if(!UseCustomRes)
            Screen.SetResolution(1280, 720, true);
        else
            Screen.SetResolution(1920, 1080, true);
    }
}
