﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OceanArea : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            other.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            Debug.Log("Enter");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            other.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
            Debug.Log("Exit");
        }
    }
}
