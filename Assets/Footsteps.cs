﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    //Lower = faster
    const float FootstepOffset = 4;
    List<AudioClip> CurrentFootsteps = new List<AudioClip>();

    [SerializeField] List<AudioClip> SandFootsteps = new List<AudioClip>();
    [SerializeField] List<AudioClip> GrassFootsteps = new List<AudioClip>();
    [SerializeField] List<AudioClip> MudFootsteps = new List<AudioClip>();

    Movement movement;
    AudioManager audioManager;

    Coroutine currentStepSound;
    float currentTimer = 0;
    float timerValue;

    int timerValueInt = 1;
    int prevTimerInt = 0;
    int footStepIndex = 0;
    public string CurrentFootstep;
    float prevSpeed = 0;
    private void Awake()
    {
        movement = GetComponent<Movement>();
        audioManager = FindObjectOfType<AudioManager>();
    }

    private void Start()
    {
        CurrentFootsteps = GrassFootsteps;
        CurrentFootstep = "grass";
    }

    public void ChangeFootstep(string footstep)
    {
        switch (footstep)
        {
            case "sand":
                CurrentFootsteps = SandFootsteps;
                break;
            case "water":
                CurrentFootsteps = MudFootsteps;
                break;
            case "grass":
                CurrentFootsteps = GrassFootsteps;
                break;
        }
    }

    private void OnDisable()
    {
        currentTimer = 0;
    }

    private void Update()
    {
        prevTimerInt = timerValueInt;
        //From rest, reset the footstep timer
        //copunt down if timer greater than 0

        if (movement.CurrentGroundSpeed > 0 && currentTimer <= 0) 
        {
            timerValue = FootstepOffset / movement.CurrentGroundSpeed;
            timerValueInt = Mathf.FloorToInt(timerValue * 100);
            currentTimer = Mathf.Clamp(timerValue, 0.2f, 1);
        }

        if (currentTimer > 0)
        {
            currentTimer -= Time.deltaTime;
        }
        if (movement.CurrentGroundSpeed > 0 && currentTimer <= 0)
        {
            AudioClip clip = CurrentFootsteps[footStepIndex];
            footStepIndex++;
            if (footStepIndex == CurrentFootsteps.Count)
                footStepIndex = 0;

            Sound s = audioManager.SoundGroups[1].GetSound(clip);
            if (s == null)
            {
                Debug.LogWarning("Sound not found");
            }
            s.volume = Random.Range(0.4f, 0.9f);
            s.pitch = Random.Range(0.6f, 1.1f);

            audioManager.PlaySound(SoundGroupName.FootSteps, clip);
        }


        //if(movement.CurrentGroundSpeed != 0 && prevSpeed != movement.CurrentGroundSpeed)
        //{
        //    if (currentStepSound == null)
        //    {
        //        currentStepSound = StartCoroutine(PlayStepSound(movement.CurrentGroundSpeed));
        //    }
        //    else
        //    {
        //        StopCoroutine(currentStepSound);
        //        currentStepSound = null;
        //    }
        //}
        //else if (movement.CurrentGroundSpeed == 0)
        //{
        //    if (currentStepSound != null)
        //    {
        //        StopCoroutine(currentStepSound);
        //        currentStepSound = null;
        //    }
        //}
        //if(prevSpeed != movement.CurrentGroundSpeed)
        //{
        //    Debug.Log(prevSpeed + " " + movement.CurrentGroundSpeed);
        //}

    }

    IEnumerator PlayStepSound(float speed)
    {
        do
        {
            yield return new WaitForSeconds(FootstepOffset / speed);
            AudioClip clip = CurrentFootsteps[Random.Range(0, CurrentFootsteps.Count)];
            Sound s = audioManager.SoundGroups[1].GetSound(clip);
            if (s == null)
            {
                Debug.LogWarning("Sound not found");
                yield break;
            }
            s.volume = Random.Range(0.5f, 0.8f);
            s.pitch = Random.Range(0.8f, 1.1f);

            audioManager.PlaySound(SoundGroupName.FootSteps, clip);
        }
        while (true);
    }
}
