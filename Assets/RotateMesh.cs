﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMesh : MonoBehaviour
{
    //Resolution where development was tested.
    const float DEVWIDTH = 1920;
    const float DEVHEIGHT = 1080;

    [SerializeField] FixedTouchField touchField;

    public float mouseSens = 100f;
    public Transform playerBody;
    float xRotation = 0f;
    float yRotation = 0f;
    float zRotation = 0f;
    float zoomSens = 1;

    Vector2 ratio;

    private void Awake()
    {
        mouseSens = TempSavedData.MouseSensitivity;
        //Set speed to be constant with screen size.
        //ratio is locked to 16:9 for now.
        ratio.x = (float)Screen.width / DEVWIDTH;
        ratio.y = (float)Screen.height / DEVHEIGHT;
    }
    // Update is called once per frame
    void Update()
    {
        float mouseX = touchField.TouchDist.x * (mouseSens - zoomSens) * Time.deltaTime;
        float mouseY = touchField.TouchDist.y * (mouseSens - zoomSens) * Time.deltaTime;

        mouseX /= ratio.x;
        mouseY /= ratio.y;

        xRotation -= mouseY;

        Vector3 mousePos = new Vector3(mouseX, mouseY);
        if (Vector3.Dot(transform.up, Vector3.up) >= 0)
        {
            transform.Rotate(transform.up, -Vector3.Dot(mousePos, Camera.main.transform.right), Space.World);
        }
        else
        {
            transform.Rotate(transform.up, Vector3.Dot(mousePos, Camera.main.transform.right), Space.World);
        }    
            transform.Rotate(Camera.main.transform.right, Vector3.Dot(mousePos, Camera.main.transform.up), Space.World);
    }
}
