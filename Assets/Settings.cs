﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Settings : MonoBehaviour
{
    [SerializeField] Slider mouseSens;
    [SerializeField] Toggle useMotion;
    [SerializeField] TextMeshProUGUI sensText;
    private void Start()
    {
        mouseSens.value = TempSavedData.MouseSensitivity;
        useMotion.isOn = TempSavedData.UseMotionControlls;
        sensText.text = TempSavedData.MouseSensitivity.ToString();
    }

    void SetSavedData()
    {
        mouseSens.value = TempSavedData.MouseSensitivity;
        useMotion.isOn = TempSavedData.UseMotionControlls;
        sensText.text = TempSavedData.MouseSensitivity.ToString();
    }

    public void SetMouseSens(float slider)
    {
        TempSavedData.MouseSensitivity = slider;
        SetSavedData();
    }

    public void SetGyroUse(bool use)
    {
        TempSavedData.UseMotionControlls = use;
        SetSavedData();
    }
}
