﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;

public class TurtleAI : SpecialAI
{
    [SerializeField] List<Transform> SpawnAreas;
    [SerializeField] List<Transform> BeachAreas;
    public Transform TargetArea;
    Transform currentSpawn;
    bool setFlee = false;
    public void Emerge()
    {
        currentSpawn = SpawnAreas[0];
        Debug.Log("Emerge");
        /*
         * Chooses randomly from the nearest beach areas 
        */
        List<Transform> transforms = BeachAreas.OrderByDescending(area => Vector3.Distance(area.position, transform.position)).ToList();

        if (BeachAreas.Count > 0)
        {
            TargetArea = transforms[BeachAreas.Count - 1];
            agent.SetDestination(transforms[BeachAreas.Count-1].position);
            Debug.Log(agent.pathEndPosition);
        }
        else
            Debug.LogWarning("No Beach Areas");

        setFlee = false;
    }

    public override void DoSpecialAction()
    {
        base.DoSpecialAction();
        if (timerDone)
        {
            ResetAnimal();
            animator.SetBool("Flee", false);
            timerDone = false;
        }
        if (currentSpawn && !setFlee)
        {
            agent.SetDestination(currentSpawn.position);
            setFlee = true;
        }
    }

    protected override void ResetAnimal()
    {
        base.ResetAnimal();
        OnChangeDetectionColor?.Invoke(Color.yellow);

        /*
         * Chooses randomly from the furthest spawn areas 
        */
        int randIndex = Random.Range(0, SpawnAreas.Count);

        if (SpawnAreas.Count > 0)
        {
            agent.Warp(SpawnAreas[randIndex].position);
            Debug.Log(SpawnAreas[randIndex].name);
        }
        else
            Debug.LogWarning("No Spawn Areas");
    }
}
