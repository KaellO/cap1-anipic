﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrocTrigger : MonoBehaviour
{
    public DialogueManager dManager;
    Dialogue currentDialogue;
    public Dialogue[] AllDialogues;
    public CharacterController playerCC;

    private void Awake()
    {
        currentDialogue = AllDialogues[0];
    }
    private void OnDisable()
    {
        dManager.OnDialogueEnd -= DialogueEnd;
    }
    public void OnEnable()
    {
        dManager.OnDialogueEnd += DialogueEnd;
        dManager.StartDialogue(currentDialogue);
    }

    public void DialogueEnd()
    {
        gameObject.SetActive(false);
        playerCC.enabled = true;
    }
}
