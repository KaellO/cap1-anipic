﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SlideShowUI : MonoBehaviour
{
    public List<GameObject> IntroSlides;
    public List<GameObject> OutroSlides;
    public List<GameObject> Slides;
    GameObject currentSlide;
    public DialogueManager dManager;
    public FadeTransitionUI fade;
    int currentIndex;
    void Start()
    {
        if (TempSavedData.CurrentLevel == 0)
            Slides = IntroSlides;
        else
            Slides = OutroSlides;

        currentSlide = Slides[0];
        currentIndex = 1;
        currentSlide.GetComponent<CanvasGroup>().alpha = 1;
        currentSlide.GetComponent<UIAnimation>().StartTween();
    }

    public void DisplayNextSlide()
    {
        if (currentIndex + 1 > Slides.Count)
        {
            currentIndex = 0;
        }

        currentSlide.GetComponent<CanvasGroup>().DOFade(0f, 0.5f);

        currentSlide = Slides[currentIndex++];

        currentSlide.GetComponent<CanvasGroup>().DOFade(1f, 0.5f);
        currentSlide.GetComponent<UIAnimation>().StartTween();
    }
}
