﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerTutorial : MonoBehaviour
{
    [SerializeField] List<TutorialPart> tutorialParts = new List<TutorialPart>();
    [SerializeField] GameObject tutorialPanel;
    [SerializeField] GameObject hole;
    [SerializeField] GameObject dialogueBox;
    [SerializeField] DialogueManager dManager;

    Queue<TutorialPart> tutorialQueue = new Queue<TutorialPart>();

    TutorialPart currentPart;

    private void Start()
    {
        dManager.OnDialogueEnd += DialogueEnd;
        tutorialQueue.Clear();

        foreach (TutorialPart val in tutorialParts)
        {
            tutorialQueue.Enqueue(val);
        }

        NextSequence();
    }

    public void NextSequence()
    {
        Debug.Log(tutorialQueue.Count);
        if (tutorialQueue.Count == 0)
        {
            tutorialPanel.SetActive(false);
            return;
        }

        tutorialPanel.SetActive(true);
        TutorialPart part = tutorialQueue.Dequeue();
        currentPart = part;

        part.Init(hole, dialogueBox);
        dManager.StartDialogue(part.DialogueSequence);
    }

    void DialogueEnd()
    {
        if (currentPart.Sequened)
            NextSequence();
        else
        {
            tutorialPanel.SetActive(false);
        }
    }
}
