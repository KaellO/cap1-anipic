﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCombiner : MonoBehaviour
{
    public void CombineMeshes()
    {
        Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;

        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;

        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();

        Debug.Log(name + " " + meshFilters.Length + " Combining Meshes!");

        Mesh finalMesh = new Mesh();

        CombineInstance[] combinedInstances = new CombineInstance[meshFilters.Length];

        for (int i = 0; i < meshFilters.Length; i++)
        {
            if (meshFilters[i].transform == transform) continue;
            if (meshFilters[i].gameObject.name == "Cube") continue;

            combinedInstances[i].subMeshIndex = 0;
            combinedInstances[i].mesh = meshFilters[i].sharedMesh;
            combinedInstances[i].transform = meshFilters[i].transform.localToWorldMatrix;
        }

        finalMesh.CombineMeshes(combinedInstances);

        GetComponent<MeshFilter>().sharedMesh = finalMesh;

        transform.rotation = oldRot;
        transform.position = oldPos;

        Transform[] children = GetComponentsInChildren<Transform>();

        //for (int i = 0; i < children.Length; i++)
        //{
        //    if (children[i].gameObject.name == "default")
        //    {
        //        children[i].gameObject.SetActive(false);
        //    }
        //}

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
