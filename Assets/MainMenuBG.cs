﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenuBG : MonoBehaviour
{
    public List<GameObject> Slides;
    GameObject currentSlide;

    int currentIndex;
    void Start()
    {
        currentSlide = Slides[0];
        currentIndex = 1;
        currentSlide.GetComponent<CanvasGroup>().alpha = 1;
        currentSlide.GetComponent<UIAnimation>().StartTween();
    }

    public void DisplayNextSlide()
    {
        if (currentIndex + 1 > Slides.Count)
        {
            currentIndex = 0;
        }
        currentSlide.GetComponent<CanvasGroup>().DOFade(0f, 0.5f);

        currentSlide = Slides[currentIndex++];

        currentSlide.GetComponent<UIAnimation>().Reset();
        currentSlide.GetComponent<CanvasGroup>().DOFade(1f, 0.5f);
        currentSlide.GetComponent<UIAnimation>().StartTween();
    }
}
