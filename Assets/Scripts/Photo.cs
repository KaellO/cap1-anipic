﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Photo : MonoBehaviour
{
    public Sprite imageSprite;
    public string Subject;
    public AnimalTriviaData trivia;
    public string FileName;
    public int Rating;
    public bool Select;
    public CameraMechanic.ZoomLevel ZoomLevel;
    public string FilePath;
    public Quest AttachedQuest;
    public CameraMechanic.Orientation Orientation;
}
