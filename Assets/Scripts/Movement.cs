﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    CharacterController characterController;
    public float speed = 12f;
    private const float gravity = -9.81f;

    Vector3 velocity;
    public float CurrentGroundSpeed;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    public void Move(Vector3 movementInput)
    {
        characterController.Move(movementInput * speed * Time.deltaTime);
        CurrentGroundSpeed = Mathf.Abs(characterController.velocity.x) 
                        + Mathf.Abs(characterController.velocity.z);
    }
    private void Update()
    {
        velocity.y += gravity * Time.deltaTime;

        characterController.Move(velocity * Time.deltaTime);
    }
}
