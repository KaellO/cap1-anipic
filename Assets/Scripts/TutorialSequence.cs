﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TutorialSequence : MonoBehaviour
{
    [SerializeField] List<TutorialPart> tutorialParts = new List<TutorialPart>();
    [SerializeField] GameObject tutorialPanel;
    [SerializeField] GameObject hole;
    [SerializeField] GameObject dialogueBox;
    [SerializeField] DialogueManager dManager;

    Queue<TutorialPart> tutorialQueue = new Queue<TutorialPart>();
    public Action OnCameraOpened;

    CameraMechanic cameraMechanic;
    QuestManager questManager;
    TutorialPart currentPart;
    PlayerController playerController;

    bool QuestComplete = false;
    float playerSpeed = 0;
    bool photoTaken = false;
    private void Start()
    {
        cameraMechanic = GameObject.FindObjectOfType<CameraMechanic>();
        questManager = GameObject.FindObjectOfType<QuestManager>();
        playerController = GameObject.FindObjectOfType<PlayerController>();
        playerSpeed = playerController.movementComp.speed;

        questManager.OnAllQuestsComplete += () => QuestComplete = true;
        cameraMechanic.OnCameraOpen += CameraTutorial;
        cameraMechanic.OnRenderPhoto += () => photoTaken = true;
        dManager.OnDialogueEnd += DialogueEnd;
        cameraMechanic.OnCameraClose += CameraClosed;
        tutorialQueue.Clear();

        foreach (TutorialPart val in tutorialParts)
        {
            tutorialQueue.Enqueue(val);
        }

        NextSequence();
    }

    private void OnDisable()
    {
        dManager.OnDialogueEnd -= DialogueEnd;
    }

    public void NextSequence()
    {
        if (tutorialQueue.Count == 0)
        {
            tutorialPanel.SetActive(false);
            return;
        }

        playerController.movementComp.speed = 0;
        tutorialPanel.SetActive(true);
        TutorialPart part = tutorialQueue.Dequeue();
        currentPart = part;

        part.Init(hole, dialogueBox);
        dManager.StartDialogue(part.DialogueSequence);
    }

    void DialogueEnd()
    {
        if (currentPart.DialogueSequence.name == "Album Tutorial" && QuestComplete)
        {
            NextSequence();
        }
        if (currentPart.Sequened)
            NextSequence();
        else
        {
            tutorialPanel.SetActive(false);
        }
        playerController.movementComp.speed = playerSpeed;
    }

    bool CameraTutorialDone = false;
    void CameraTutorial()
    {
        if (CameraTutorialDone)
            return;
        //Standard Progression
        if (currentPart.DialogueSequence.name == "Camera Tutorial")
        {
            NextSequence();
            CameraTutorialDone = true;
        }
        //If the player pressed the camera button early do this
        else
        {
            foreach (TutorialPart part in tutorialQueue.ToArray())
            {
                if (part.DialogueSequence.name == "CamButton Tutorial")
                    break;

                Debug.Log(part.DialogueSequence.name);
                tutorialQueue.Dequeue();
            }
            NextSequence();
            OnCameraOpened?.Invoke();
            CameraTutorialDone = true;
        }
    }

    void CameraClosed()
    {
        if (currentPart.DialogueSequence.name == "CamExit Tutorial" && photoTaken == true)
        {
            NextSequence();
            photoTaken = false;
        }

        else if (QuestComplete)
            NextSequence();
    }
}
