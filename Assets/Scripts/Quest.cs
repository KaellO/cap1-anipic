﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest 
{
    [SerializeField] QuestData questData;
    public QuestData QuestData
    {
        get { return questData; }
    }
    public bool Complete;
}
