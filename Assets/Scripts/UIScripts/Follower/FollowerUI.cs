﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FollowerUI : MonoBehaviour
{

    public Text FollowerAddedText;
    public Text FollowerTotalText;
    public Text ProfileFollowerTotalText;
    public Text TotalFollowersProfile;
    public GameObject FollowerAddedUI;
    public GameObject OkButton;
    public GameObject ProfileUI;
    public GameObject HomeUI;
    // Start is called before the first frame update
    void Start()
    {
        FollowerAddedText.text = TempSavedData.addedFollowers.ToString();
        FollowerTotalText.text = TempSavedData.totalFollowers.ToString();
        FollowerAddedUI.SetActive(true);
        StartCoroutine(AddFollowersAnimation());
        TotalFollowersProfile.text = TempSavedData.totalFollowers.ToString();
    }

    public void CheckTutorial()
    {

        switch (TempSavedData.CurrentLevel)
        {
            case 1:
                SceneManager.LoadSceneAsync("TutorialSceneFollower", LoadSceneMode.Additive);
                break;
        }
    }

    public void GoToScene()
    {
        SceneManager.LoadScene("DialogueScene");
    }

    public void HideFollowerUI()
    {
        FollowerAddedUI.SetActive(false);
    }

    /*
     * Add if player taps, skip animation and set total and added values properly
     * total += added
     * added = 0
     */
    float waitTime = 0.01f;
    public void Skip()
    {
        StopAllCoroutines();
        TempSavedData.totalFollowers += TempSavedData.addedFollowers;
        TempSavedData.addedFollowers = 0;
        FollowerTotalText.text = TempSavedData.totalFollowers.ToString();
        FollowerAddedText.text = TempSavedData.addedFollowers.ToString();
        OkButton.SetActive(true);
    }
    IEnumerator AddFollowersAnimation()
    {
        yield return new WaitForSeconds(1);
        float totalAddedFollowers = TempSavedData.addedFollowers;
        while(TempSavedData.addedFollowers > 0)
        {
            TempSavedData.totalFollowers++;
            FollowerTotalText.text = TempSavedData.totalFollowers.ToString();
            TempSavedData.addedFollowers--;
            FollowerAddedText.text = TempSavedData.addedFollowers.ToString();
            yield return new WaitForSeconds(waitTime);
        }
        OkButton.SetActive(true);
    }

    public void DisplayProfileUI()
    {
        ProfileFollowerTotalText.text = FollowerTotalText.text;
        if (!ProfileUI.activeInHierarchy)
        {
            ProfileUI.SetActive(true);
        }
        else 
        {
            ProfileUI.SetActive(false);
        }
    }
    public void DisplayHomePanel()
    {
        ProfileUI.SetActive(false);
        HomeUI.SetActive(true);
    }
}
