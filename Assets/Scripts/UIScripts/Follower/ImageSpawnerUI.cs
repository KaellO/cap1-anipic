﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageSpawnerUI : MonoBehaviour
{
    public float WaitTime;
    public float ImageSpawnTotal;//26
    public List<GameObject> ImagesList;

    public RectTransform Panel;
    public float PanelMariginX;
    public float PanelMariginY;

    void Start()
    {
        StartCoroutine(SpawnImage());
    }


    IEnumerator SpawnImage()
    {
        for (int i = 0; i < ImageSpawnTotal; i++)
        {
            GameObject imageToSpawn = ImagesList[Random.Range(0, ImagesList.Count)];

            Vector3[] panelPoint = new Vector3[4];
            Panel.GetWorldCorners(panelPoint);
            //panelPoint[0] is bottom right,[1] is bottom left,[2] is upper left

            //Vector3 spawnPosition = GetBottomLeftCorner(Panel) - new Vector3(Random.Range(0, Panel.rect.x), Random.Range(0, Panel.rect.y), 0);
            Vector3 spawnPosition = new Vector3(Random.Range(panelPoint[1].x, panelPoint[0].x) + PanelMariginX, Random.Range(panelPoint[1].y, panelPoint[2].y) + PanelMariginY, 0);

            //print("Panel X:" + Panel.rect.x + " Panel Y:" + Panel.rect.y);
            //print("Spawn image at position: " + spawnPosition);

            GameObject spawnObj = Instantiate(imageToSpawn, spawnPosition, imageToSpawn.transform.rotation, Panel);
            yield return new WaitForSeconds(WaitTime);
        }
    }

    Vector3 GetBottomLeftCorner(RectTransform rt)
    {
        Vector3[] v = new Vector3[4];
        rt.GetWorldCorners(v);
        //print(v[3]);
        return v[3];
    }
}
