﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PhotoThumbnailUI : MonoBehaviour
{
    public TextMeshProUGUI NameText;
    public Image ThumbnailPhoto;
    public GameObject Thumbnail;
    public GameObject RatingPanel;
    public GameObject RatingPrefab;
    public GameObject Select;
    public Photo photo;

    public Action PhotoSelected;

    public void OnEnable()
    {
        Select.SetActive(photo.Select);
    }
    public void SetupViewButton(AlbumUI album)
    {
        Thumbnail.GetComponent<Button>().onClick.RemoveAllListeners();
        Thumbnail.GetComponent<Button>().onClick.AddListener(() => album.DisplayImage(photo));
    }

    public void SetupSubmitButton(AlbumUI album)
    {
        Thumbnail.GetComponent<Button>().onClick.RemoveAllListeners();
        Thumbnail.GetComponent<Button>().onClick.AddListener(SelectPhoto);
    }

    public void SelectPhoto()
    {
        if(PhotoManager.instance.SelectPhoto(photo))
        {
            Select.SetActive(!Select.activeSelf);

        }
    }

    public void SetupRating(Photo photo)
    {
        for (int i = 0; i < photo.Rating; i++)
        {
            GameObject rating = Instantiate(RatingPrefab, RatingPanel.transform);
            rating.transform.position = RatingPanel.transform.position;
        }
    }

    public void OnDisable()
    {
        photo.Select = false;
        Select.SetActive(photo.Select);
    }
}
