﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ObjectiveContainerUI : MonoBehaviour
{
    public TextMeshProUGUI QuestText;
    public GameObject QuestIndicator;

    public void CompleteQuest()
    {
        QuestText.fontStyle = FontStyles.Strikethrough;
        QuestIndicator.SetActive(true);
    }
}
