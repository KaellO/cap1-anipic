﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestListContainerUI : MonoBehaviour
{
    public GameObject QuestPanel;
    public Dictionary<Requirement, GameObject> Objectives = new Dictionary<Requirement, GameObject>();
    public void CreateObjective(GameObject questPrefab, Requirement requirementData)
    {
        GameObject Objective = Instantiate(questPrefab, QuestPanel.transform);
        Objectives.Add(requirementData, Objective);

        Objective.GetComponent<ObjectiveContainerUI>().QuestText.text = requirementData.Description;
        Objective.GetComponent<ObjectiveContainerUI>().QuestIndicator.SetActive(false);
    }
}
