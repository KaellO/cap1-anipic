﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
public class AlbumUI : MonoBehaviour
{
    [SerializeField] MainGameUI parentUI;
    [SerializeField] GameObject imageContainer;
    [SerializeField] GameObject imagePrefab;
    [SerializeField] CameraMechanic cameraMechanic;
    [SerializeField] GameObject IndividualImagePanel;
    [SerializeField] GameObject SubmitButton;
    Stack<GameObject> images = new Stack<GameObject>();
    string[] files = null;

    private void OnEnable()
    {
        foreach (GameObject image in images)
        {
            image.GetComponentInChildren<Button>().interactable = true;
        }
    }

    public void Init()
    {
        cameraMechanic.OnPhotoTaken += GenerateImageObject;
        PhotoManager.AllPhotosSelected += AllMainPhotosSelected;
        PhotoManager.SetPhotoObject += SetPhotoInteractable;
    }

    private void OnDestroy()
    {
        PhotoManager.AllPhotosSelected -= AllMainPhotosSelected;
        PhotoManager.SetPhotoObject -= SetPhotoInteractable;
    }

    private void OnDisable()
    {
        AllMainPhotosSelected(false);
    }

    void AllMainPhotosSelected(bool c)
    {
        SubmitButton.SetActive(c);
        //if (c)
        //    foreach (GameObject image in images)
        //    {
        //        if (!image.GetComponent<Photo>().Select)
        //            image.GetComponentInChildren<Button>().interactable = false;
        //    }
        //else
        //    foreach (GameObject image in images)
        //    {
        //        image.GetComponentInChildren<Button>().interactable = true;
        //    }
    }

    public void SetPhotoInteractable(Photo photo, bool c)
    {
        foreach (GameObject image in images)
        {
            if (image.GetComponent<Photo>() == photo)
                image.GetComponentInChildren<Button>().interactable = c;
        }
    }

    public void SetUpAlbumView()
    {
        foreach (GameObject image in images)
        {
            image.GetComponent<PhotoThumbnailUI>().SetupViewButton(this);
        }
    }
    void GenerateImageObject(GameObject image, Texture2D texture)
    {
        //files = Directory.GetFiles(Application.dataPath + "/PhotosTaken/", "*.png");
        //Texture2D texture = GetScreenShotTexture(files[files.Length - 1]);

        Sprite spr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                                    new Vector2(0.5f, 0.5f));

        Photo photoComp = image.GetComponent<Photo>();
        PhotoThumbnailUI photoUI = image.GetComponent<PhotoThumbnailUI>();

        photoComp.imageSprite = spr;
        image.transform.SetParent(imageContainer.transform, false);
        image.SetActive(true);
        images.Push(image);
        image.GetComponent<RectTransform>().SetAsFirstSibling();

        photoUI.ThumbnailPhoto.sprite = spr;
        photoUI.SetupRating(photoComp);
        photoUI.NameText.text = photoComp.Subject;
    }

    public void SetUpSubmition()
    {
        foreach(GameObject photo in images)
        {
            photo.GetComponent<PhotoThumbnailUI>().SetupSubmitButton(this);
            if (photo.GetComponent<Photo>().AttachedQuest.QuestData == null)
                photo.GetComponentInChildren<Button>().interactable = false;
        }
    }

    Texture2D GetScreenShotTexture(string filePath)
    {
        Texture2D texture = null;
        byte[] fileBytes;
        if (File.Exists(filePath))
        {
            fileBytes = File.ReadAllBytes(filePath);
            texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
            texture.LoadImage(fileBytes);
        }
        return texture;
    }

    //When the user clicks on the image
    public void DisplayImage(Photo photo)
    {
        IndividualImagePanel.GetComponent<IndivImageUI>().SetupImage(photo);
        parentUI.ActivatePanel(IndividualImagePanel);
    }

    public void SelectPhotoForSubmission(Photo photo)
    {
        photo.Select = true;
    }


}
