﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IndivImageUI : MonoBehaviour
{
    public Photo Photo;
    public Image image;
    public TextMeshProUGUI Name;
    public TextMeshProUGUI ScientificName;
    public TextMeshProUGUI Description;
    public GameObject RatingPanel;
    public GameObject RatingPrefab;

    List<GameObject> rating = new List<GameObject>();

    public void SetupImage(Photo photo)
    {
        image.sprite = photo.imageSprite;
        Name.text = photo.Subject;

        if(photo.trivia)
        {
            ScientificName.text = photo.trivia.ScientificName;
            Description.text = photo.trivia.Description;
        }

        for (int i = 0; i < photo.Rating; i++)
        {
            rating.Add(Instantiate(RatingPrefab, RatingPanel.transform));
        }
    }

    private void OnDisable()
    {
        foreach (GameObject go in rating)
        {
            Destroy(go);
        }

        rating.Clear();
    }
}
