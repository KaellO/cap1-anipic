﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainGameUI : MonoBehaviour
{
    //[SerializeField] GameObject gameUIPanel;
    //[SerializeField] GameObject cameraPanel;
    //[SerializeField] GameObject albumPanel;
    //[SerializeField] GameObject individualImagePanel;
    //[SerializeField] GameObject finalScorePanel;

    [SerializeField] List<GameObject> panels = new List<GameObject>();
    private void Start()
    {
        //panels.Add(gameUIPanel);
        //panels.Add(cameraPanel);
        //panels.Add(albumPanel);
        //panels.Add(individualImagePanel);
        //panels.Add(finalScorePanel);
        if (TempSavedData.CurrentLevel == 0)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = false;
            StartCoroutine(delay());
        }
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(2f);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
    public void ActivatePanel(GameObject panel)
    {
        foreach(GameObject p in panels)
        {
            p.SetActive(p == panel);
        }
    }
}
