﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

// handles behavior of quests
public class QuestManager : MonoBehaviour
{
    [SerializeField] CameraMechanic player;
    public List<Quest> quests;
    public Action<Quest[]> OnInitializeQuest;
    public Action<Quest> OnQuestComplete;
    public Action OnAllQuestsComplete;
    public Action<Quest> OnSideRecComplete;
    public Action<Photo> OnThreeStarPhoto;

    public static QuestManager instance;

    int mainQuestsComplete;
    [HideInInspector] public Quest[] mainQuests;
    [HideInInspector] public Quest[] sideQuests;

    [SerializeField] List<Photo> validPhotos = new List<Photo>();
    List<Quest> questsCompleted = new List<Quest>();
    private void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(instance);

        OnInitializeQuest?.Invoke(quests.ToArray());

        player.OnEvaluatePhoto += EvaluatePhoto;

        mainQuests = quests.Where(
            quest => quest.QuestData.Type == QuestData.QuestType.MainQuest).ToArray();

        sideQuests = quests.Where(
            quest => quest.QuestData.Type == QuestData.QuestType.SideQuest).ToArray();
    }

    void EvaluatePhoto(Photo photo)
    {
        //Add photo to list if it fulfills a quest and its the first 
        if (validPhotos.FirstOrDefault(
            p => p.AttachedQuest.QuestData == photo.AttachedQuest.QuestData) == null)
        {
            validPhotos.Add(photo);
            Debug.Log(validPhotos.FirstOrDefault(
            p => p.AttachedQuest.QuestData == photo.AttachedQuest.QuestData));
        }
        //Check quests completed
        IEnumerable<Quest> MainRequirementComplete = quests.Where(
            quest => quest.QuestData.MainRequirement.RequiredSubject == photo.Subject);

        //Change to loop if more side reqs, but for now its only zoom
        IEnumerable<Quest> SideRequirementComplete = MainRequirementComplete.Where(
            quest => quest.QuestData.SideRequirements.Length > 0  &&
            quest.QuestData.SideRequirements[0].RequiredZoom == photo.ZoomLevel);

        //this literally runs once but im too lazy to fix it whatever
        foreach (Quest quest in MainRequirementComplete)
        {
            quest.Complete = true;
            photo.Rating += 1;
            if (quest.QuestData.SideRequirements.Length <= 0)
            {
                photo.Rating += 1;
            }
            if (photo.Orientation == CameraMechanic.Orientation.Front)
            {
                photo.Rating += 1;
            }
            //if (quest.QuestData.Type == QuestData.QuestType.MainQuest && validPhotos.Contains(photo))
            //{
            //    mainQuestsComplete++;
            //}
            photo.AttachedQuest = quest;
            OnQuestComplete?.Invoke(quest);

            int mainQuestsComplete = 0;
            foreach (Quest q in mainQuests)
            {
                if (q.Complete)
                    mainQuestsComplete++;
            }

            if (mainQuests.Length == mainQuestsComplete)
            {
                OnAllQuestsComplete?.Invoke();
            }

            if(photo.Rating == 3)
                OnThreeStarPhoto?.Invoke(photo);
        }


        foreach (Quest quest in SideRequirementComplete)
        {
            photo.Rating++;

            //Temp change to loop
            OnSideRecComplete?.Invoke(quest);
            if(photo.Rating == 3)
                OnThreeStarPhoto?.Invoke(photo);
        }
    }
}
