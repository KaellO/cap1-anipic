﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

// Tells Unity to use this Editor class with the WaveManager script component.
[CustomEditor(typeof(TutorialSequence)), CanEditMultipleObjects]
public class TutorialEditor : Editor
{

    // This will contain the <TutorialPart> array of the WaveManager. 
    SerializedProperty tutorialParts;

    // The Reorderable List we will be working with 
    ReorderableList list;

    private void OnEnable()
    {
        // Gets the wave property in WaveManager so we can access it. 
        tutorialParts = serializedObject.FindProperty("tutorialParts");

        // Initialises the ReorderableList. We are creating a Reorderable List from the "wave" property. 
        // In this, we want a ReorderableList that is draggable, with a display header, with add and remove buttons        
        list = new ReorderableList(serializedObject, tutorialParts, true, true, true, true);

        list.elementHeight = 90;

        list.drawElementCallback = DrawListItems; // Delegate to draw the elements on the list
        list.drawHeaderCallback = DrawHeader; // Skip this line if you set displayHeader to 'false' in your ReorderableList constructor.
    }

    // Draws the elements on the list
    void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index); // The element in the list

        EditorGUI.LabelField(new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight), "DialogueSequence");
        EditorGUI.PropertyField(
            new Rect(rect.x + 130, rect.y, 150, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("DialogueSequence"),
            GUIContent.none
        );

        EditorGUI.LabelField(new Rect(rect.x, rect.y + 20, 120, EditorGUIUtility.singleLineHeight), "HoleLocation");
        EditorGUI.PropertyField(
            new Rect(rect.x + 130, rect.y + 20, 150, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("HoleLocation"),
            GUIContent.none
        );

        EditorGUI.LabelField(new Rect(rect.x, rect.y + 40, 120, EditorGUIUtility.singleLineHeight), "HoleSize");
        EditorGUI.PropertyField(
            new Rect(rect.x + 130, rect.y + 40, 150, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("HoleSize"),
            GUIContent.none
        );

        EditorGUI.LabelField(new Rect(rect.x, rect.y + 60, 120, EditorGUIUtility.singleLineHeight), "Sequenced");
        EditorGUI.PropertyField(
            new Rect(rect.x + 130, rect.y + 60, 150, EditorGUIUtility.singleLineHeight),
            element.FindPropertyRelative("Sequened"),
            GUIContent.none
        );
    }

    //Draws the header
    void DrawHeader(Rect rect)
    {
        string name = "Tutorial Sequence";
        EditorGUI.LabelField(rect, name);
    }

    //This is the function that makes the custom editor work
    public override void OnInspectorGUI()
    {
        //Hide actual inspector call
        base.OnInspectorGUI();

        serializedObject.Update();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }
}