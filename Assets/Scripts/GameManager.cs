﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject PhotoUI;
    [SerializeField] CompendiumUI AnimalCompendium;
    [SerializeField] QuestManager QuestManager;

    //First List is the level, second are the quests for said level
    [System.Serializable]
    public struct QuestList
    {
        public List<Quest> questList;
    }
    public List<QuestList> allQuests = new List<QuestList>();

    void Awake()
    {
        PhotoUI.GetComponent<AlbumUI>().Init();
        AnimalCompendium.Init();

        //This is scuffed, ask for consultations

        QuestList quests = allQuests[TempSavedData.CurrentLevel];
        foreach (Quest quest in quests.questList)
        {
            QuestManager.quests.Add(quest);
        }

        switch (TempSavedData.CurrentLevel)
        {
            case 0:
                SceneManager.LoadSceneAsync("TutorialScene", LoadSceneMode.Additive);
                break;
        }

    }

    public void GoToFollowScene()
    {
        SceneManager.LoadScene("FollowerScene");
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void UnPause()
    {
        Time.timeScale = 1;
    }
}
