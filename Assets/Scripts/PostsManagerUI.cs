﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PostsManagerUI : MonoBehaviour
{
    List<GameObject> Posts = new List<GameObject>();
    [SerializeField] GameObject postPrefab;
    [SerializeField] GameObject PostsPanel;
    public SocMedManager smManager;
    float[] FollowerMult = { 1, 1.5f, 2, 3, 4 };
    private void Start()
    {
        //Create Posts
        for (int i = 0; i < smManager.Photos.Count; i++)
        {
            int random = Random.Range(95, 125);
            GameObject post = Instantiate(postPrefab, PostsPanel.transform);
            post.GetComponent<PostUI>().MainImage.sprite = smManager.Photos[i].imageSprite;
            post.GetComponent<PostUI>().LikesText.text = Mathf.Ceil(smManager.Photos[i].Rating * random
                * FollowerMult[TempSavedData.CurrentLevel - 1]).ToString();
            post.GetComponent<PostUI>().TitleText.text = smManager.Photos[i].Subject;
            post.GetComponent<RectTransform>().SetAsFirstSibling();
            Posts.Add(post);
            post.SetActive(true);
        }
    }
}
