﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;    
public class SwipeCam : MonoBehaviour
{
    //Resolution where development was tested.
    const float DEVWIDTH = 1920;
    const float DEVHEIGHT = 1080;

    [SerializeField] TextMeshProUGUI sensText;
    [SerializeField] FixedTouchField touchField;

    public float mouseSens = 100f;
    public Transform playerBody;
    float xRotation = 0f;

    float zoomSens = 1;

    Vector2 ratio;

    private void Awake()
    {
        mouseSens = TempSavedData.MouseSensitivity;
        //Set speed to be constant with screen size.
        //ratio is locked to 16:9 for now.
        ratio.x = (float)Screen.width / DEVWIDTH;
        ratio.y = (float)Screen.height / DEVHEIGHT;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = touchField.TouchDist.x * (mouseSens - zoomSens) * Time.deltaTime;
        float mouseY = touchField.TouchDist.y * (mouseSens - zoomSens) * Time.deltaTime;

        mouseX /= ratio.x;
        mouseY /= ratio.y;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    public void OnCameraZoom(float value)
    {
        zoomSens = TempSavedData.MouseSensitivity - value;
        zoomSens = Mathf.Clamp(zoomSens, 1, 100);
    }

    public void ChangeSensitivity(float value)
    {
        mouseSens = value;
        sensText.text = mouseSens.ToString();
        TempSavedData.MouseSensitivity = value;
    }
}
