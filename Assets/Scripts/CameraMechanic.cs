﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;

public class CameraMechanic : MonoBehaviour
{
    public enum ZoomLevel
    {
        Far,
        Mid,
        Close
    }
    public enum Orientation
    {
        Front,
        Back
    }
    Camera mainCamera;
    public ZoomLevel zoomLevel;
    public Orientation orientation;
    ZoomLevel prevZoomLevel;
    Orientation prevOrientation;

    [Header("Camera Settings")]
    [SerializeField] float closeDistance;
    [SerializeField] float farDistance;
    [SerializeField] float maxDistance;
    [Header("UI")]
    [SerializeField] TextMeshProUGUI zoomText;
    [SerializeField] TextMeshProUGUI orienText;    
    [SerializeField] Slider zoomSlider;
    [SerializeField] GameObject cameraPanel;
    [SerializeField] GameObject animalNamePanel;

    [SerializeField] GameObject shutter1;
    [SerializeField] GameObject shutter2;
    public bool useGyro;

    public Action<GameObject, Texture2D> OnPhotoTaken;
    public Action OnCameraOpen;
    public Action<Photo> OnEvaluatePhoto;
    public Action OnCameraClose;
    public Action OnRenderPhoto;

    [SerializeField] GameObject photoPrefab;
    [SerializeField] SwipeCam swipeCam;
    [SerializeField] LayerMask ObstacleMask;
    PlayerController playerController;
    AudioManager audioManager;

    /*
     * Photos not saved for now, fix when proper saving has been implemented
     */
    private void Awake()
    {
        mainCamera = Camera.main;
        audioManager = FindObjectOfType<AudioManager>();


        if (useGyro)
            mainCamera.GetComponent<GyroCam>().enabled = true;
        zoomSlider.value = zoomSlider.maxValue;

        mainCamera.fieldOfView = 60;
        swipeCam.OnCameraZoom(60);
        mainCamera.GetComponent<GyroCam>().enabled = false;
        playerController = GetComponent<PlayerController>();
    }
    public void Init()
    {
        if (useGyro)
            mainCamera.GetComponent<GyroCam>().enabled = true;
        zoomSlider.value = zoomSlider.maxValue;
        OnCameraOpen?.Invoke();
        //Set up
        RaycastHit hit;
        if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, 100.0f, LayerMask.GetMask("Animal")))
        {
            if (!CheckForObstacle(hit))
            {
                return;
            }
            EvaluateDistance(hit.distance);
            zoomText.text = zoomLevel.ToString();

            animalNamePanel.SetActive(true);
            animalNamePanel.GetComponentInChildren<TextMeshProUGUI>().text = hit.collider.name;
        }
        playerController.enabled = false;
        GetComponent<Footsteps>().enabled = false;
    }


    public void TakePhoto()
    {
        RaycastHit hit;
        if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, 100.0f, LayerMask.GetMask("Animal")))
        {
            if (!CheckForObstacle(hit))
            {
                return;
            }
            //Generate Image
            string timeStamp = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");

            GameObject photo = Instantiate(photoPrefab);
            Photo imageTaken = photo.GetComponent<Photo>();
            imageTaken.Subject = hit.collider.name;
            imageTaken.FileName = hit.collider.name + " " + timeStamp + ".png";
            //imageTaken.FilePath = Application.dataPath + "/PhotosTaken(Temp)/" + imageTaken.FileName;
            imageTaken.ZoomLevel = zoomLevel;
            imageTaken.Orientation = orientation;
            imageTaken.trivia = hit.collider.GetComponent<AnimalAI>().triviaData;

            OnEvaluatePhoto?.Invoke(imageTaken);
            photo.SetActive(false);
            PhotoManager.instance.AllPhotos.Add(photo);
            StartCoroutine(TakeShot(imageTaken.FileName, photo));
            EvaluateDistance(hit.distance);
        }
        else
        {
            GameObject photo = Instantiate(photoPrefab);
            Photo imageTaken = photo.GetComponent<Photo>();
            imageTaken.Subject = "No Subject";
            StartCoroutine(TakeShot("NoSubject", photo));
        }
    }

    private void Update()
    {
        CheckRayDist(mainCamera.transform.position, mainCamera.transform.forward);
    }
    public void CheckRayDist(Vector3 position, Vector3 rotation)
    {
        RaycastHit hit;
        if (Physics.Raycast(position, rotation, out hit, 100.0f, LayerMask.GetMask("Animal")))
        {
            if (!CheckForObstacle(hit))
            {
                return;
            }

            float distance = hit.distance * (mainCamera.fieldOfView / 3);
            if (distance > maxDistance)
                return;
            EvaluateDistance(hit.distance);

            if (prevZoomLevel != zoomLevel || zoomText.text == "")
                zoomText.text = zoomLevel.ToString();

            animalNamePanel.SetActive(true);
            animalNamePanel.GetComponentInChildren<TextMeshProUGUI>().text = hit.collider.name;
            

            Vector3 dir = (transform.position - hit.transform.position).normalized;
            float dot = Vector3.Dot(dir, hit.transform.forward);
            if (dot > 0)
            {
                orientation = Orientation.Front;
            }
            else
                orientation = Orientation.Back;

            if (prevOrientation != orientation || orienText.text == "")
            {
                orienText.text = orientation.ToString();
                if (orientation == Orientation.Back)
                {
                    orienText.color = new Color(1, 0, 0);
                }
                if (orientation == Orientation.Front)
                {
                    orienText.color = new Color(0, 1, 0);
                }
            }
        }
        else
        {
            animalNamePanel.SetActive(false);
            zoomText.text = "";
            orienText.text = "";
        }
    }

    bool CheckForObstacle(RaycastHit hit)
    {
        float distToTarget = Vector3.Distance(transform.position, hit.collider.transform.position);
        Vector3 dirToTarget = (hit.collider.transform.position - transform.position).normalized;
        //Check for obstacles
        RaycastHit obstacle;
        if (Physics.Raycast(transform.position, dirToTarget, out obstacle, distToTarget, ObstacleMask))
        {
            return false;
        }
        return true;
    }

    IEnumerator TakeShot(string fileName, GameObject photo)
    {
        OnRenderPhoto?.Invoke();
        cameraPanel.SetActive(false);
        //ScreenCapture.CaptureScreenshot(Application.dataPath + "/PhotosTaken(Temp)/" + fileName);

        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);

        yield return new WaitForEndOfFrame();
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();

        OnPhotoTaken?.Invoke(photo, texture);

        yield return new WaitForEndOfFrame();
        PlayShutter();
        cameraPanel.SetActive(true);
    }

    void PlayShutter()
    {
        shutter1.transform.DOLocalMoveY(-112.5f, 0.5f).OnComplete(() => shutter1.transform.DOLocalMoveY(-339.27f, 0.5f));
        shutter2.transform.DOLocalMoveY(112.5f, 0.5f).OnComplete(() => shutter2.transform.DOLocalMoveY(339.27f, 0.5f));
        audioManager.PlaySound(SoundGroupName.Camera, "Shutter");
    }

    void EvaluateDistance(float dist)
    {
        float distance = dist * (mainCamera.fieldOfView / 3);

        if (distance < closeDistance) zoomLevel = ZoomLevel.Close;
        else if (distance >= closeDistance && distance < farDistance) zoomLevel = ZoomLevel.Mid;
        else zoomLevel = ZoomLevel.Far;
    }

    void EvaluateOrientation(float angle)
    {
        if (angle < 60) orientation = Orientation.Front;
        else orientation = Orientation.Back;
    }

    private void LateUpdate()
    {
        prevZoomLevel = zoomLevel;
        prevOrientation = orientation;
    }

    public void Zoom(float value)
    {
        mainCamera.fieldOfView = value;
        swipeCam.OnCameraZoom(value);
    }

    public void DisableUI()
    {
        mainCamera.fieldOfView = 60;
        swipeCam.OnCameraZoom(60);
        mainCamera.GetComponent<GyroCam>().enabled = false;
        playerController.enabled = true;
        GetComponent<Footsteps>().enabled = true;
        OnCameraClose?.Invoke();
    }

    public void OnZoomClick()
    {
        audioManager.PlaySound(SoundGroupName.Camera, "Zoom");
    }
    public void OnZooMRelease()
    {
        audioManager.StopSound("Zoom");
    }
}
