﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SocMedManager : MonoBehaviour
{
    SaveObject saveData;
    public List<Photo> Photos = new List<Photo>();
    void Awake()
    {
        /*
         * SaveData is for json files that will be used in the future for permanent saves
         * For now, we transfer data using the static script "TempSavedData"
         */
        //if (File.Exists(Application.dataPath + "/SaveFiles/" + "/save.txt"))
        //{
        //    string saveString = File.ReadAllText(Application.dataPath + "/SaveFiles/" + "/save.txt");
        //    saveData = JsonUtility.FromJson<SaveObject>(saveString);
        //}

        foreach(Photo photo in TempSavedData.photoData)
        {
            Photos.Add(photo);
            Debug.Log(photo.FileName);
        }

    }
}
