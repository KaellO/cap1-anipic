﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Side Requirement", menuName = "Side Requirement Data")]
public class SideRequirement : Requirement
{
    [Header("Main Requirement")]
    public string RequiredSubject;

    [Header("Side Requirement")]
    public CameraMechanic.ZoomLevel RequiredZoom;
}
