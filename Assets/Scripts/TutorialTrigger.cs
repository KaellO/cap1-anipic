﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    [SerializeField] TutorialSequence tutorialSequence;

    private void Start()
    {
        tutorialSequence.OnCameraOpened += () => Destroy(this.gameObject);
    }
    private void OnDisable()
    {
        tutorialSequence.OnCameraOpened -= () => Destroy(this.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        tutorialSequence.NextSequence();
        Destroy(this.gameObject);
    }
}
