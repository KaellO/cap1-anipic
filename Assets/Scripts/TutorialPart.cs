﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TutorialPart
{
    public Dialogue DialogueSequence;
    public Transform HoleLocation;
    public Vector3 HoleSize = Vector3.one;
    public bool Sequened;
    public enum BoxLocation
    {
        Top,
        Bot
    };

    public BoxLocation location;

    public void Init(GameObject hole, GameObject box)
    {
        if (HoleLocation)
            hole.transform.position = HoleLocation.position;

        hole.transform.localScale = HoleSize;
        SetBoxLocation(box);
    }

    void SetBoxLocation(GameObject box)
    {
        switch (location)
        {
            case BoxLocation.Top:
                SetDialogueTop(box);
                break;
            case BoxLocation.Bot:
                SetDialogueBottom(box);
                break;
        }
    }

    void SetDialogueTop(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0.5f, 1);
        uitransform.anchorMax = new Vector2(0.5f, 1);
        uitransform.pivot = new Vector2(0.5f, 1);


        uiObject.transform.position = new Vector3(Screen.width * 0.5f, Screen.height - 50, 0);
    }

    void SetDialogueBottom(GameObject uiObject)
    {
        RectTransform uitransform = uiObject.GetComponent<RectTransform>();

        uitransform.anchorMin = new Vector2(0.5f, 0);
        uitransform.anchorMax = new Vector2(0.5f, 0);
        uitransform.pivot = new Vector2(0.5f, 0);

        uiObject.transform.position = new Vector3(Screen.width * 0.5f, 50, 0);
    }
}

