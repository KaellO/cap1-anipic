﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class QuestUI : MonoBehaviour
{
    RectTransform rectTransform;
    [SerializeField] QuestManager qManager;
    [SerializeField] GameObject questContainerPrefab;
    [SerializeField] GameObject mainQuestPanel;
    [SerializeField] GameObject sideQuestPanel;
    [SerializeField] GameObject turnInButton;
    [SerializeField] GameObject showButton;
    [SerializeField] GameObject hideButton;
    [SerializeField] GameObject alertIndicator;

    [Header("IndividualQuestPrefabs")]
    [SerializeField] GameObject MainObjectivePrefab;
    [SerializeField] GameObject SideObjectivePrefab;

    [Header("Animation")]
    [SerializeField] float hidePosX;
    [SerializeField] float showPosX;

    [Header("QuestPanels")]
    [SerializeField] GameObject MainQuestButton;
    [SerializeField] GameObject SideQuestButton;
    [SerializeField] GameObject MainQuestParentPanel;
    [SerializeField] Image MainQuestButtonAlpha;
    [SerializeField] Image SideQuestButtonAlpha;
    [SerializeField] GameObject ParentPanel;

    Dictionary<Quest, GameObject> QuestContainers = new Dictionary<Quest, GameObject>();
    void Awake()
    {
        qManager.OnInitializeQuest += SetupQuests;
        qManager.OnQuestComplete += QuestComplete;
        qManager.OnSideRecComplete += SideReqComplete;
        qManager.OnAllQuestsComplete += AllQuestsComplete;
        rectTransform = GetComponent<RectTransform>();
    }
    void SetupQuests(Quest[] quests)
    {
        foreach(Quest quest in quests)
        {
            //Create the main quest container (contains the main and side requirements)
            GameObject questObject = Instantiate(questContainerPrefab);
            questObject.transform.SetParent(rectTransform.transform, false);
            QuestContainers.Add(quest, questObject);

            questObject.GetComponent<QuestListContainerUI>().CreateObjective(MainObjectivePrefab, quest.QuestData.MainRequirement);

            foreach (SideRequirement sideReq in quest.QuestData.SideRequirements)
            {
                questObject.GetComponent<QuestListContainerUI>().CreateObjective(SideObjectivePrefab, sideReq);
            }

            if (quest.QuestData.Type == QuestData.QuestType.MainQuest)
                questObject.transform.SetParent(mainQuestPanel.transform);
            else if (quest.QuestData.Type == QuestData.QuestType.SideQuest)
            {
                questObject.transform.SetParent(sideQuestPanel.transform);
            }
        }
    }

    void QuestComplete(Quest quest)
    {
        //Grab quest
        if (QuestContainers.TryGetValue(quest, out GameObject questContainer))
        {
            if (questContainer.GetComponent<QuestListContainerUI>().Objectives.TryGetValue(quest.QuestData.MainRequirement, out GameObject requirementPanel))
            {
                requirementPanel.GetComponent<ObjectiveContainerUI>().CompleteQuest();
            }
        }
        alertIndicator.SetActive(true);
    }

    void AllQuestsComplete()
    {
        turnInButton.SetActive(true);
    }
    void SideReqComplete(Quest quest)
    {
        //Grab quest
        if (QuestContainers.TryGetValue(quest, out GameObject questContainer))
        {
            if (questContainer.GetComponent<QuestListContainerUI>().Objectives.TryGetValue(quest.QuestData.SideRequirements[0], out GameObject srequirementPanel))
            {
                srequirementPanel.GetComponent<ObjectiveContainerUI>().CompleteQuest();
            }
        }
        alertIndicator.SetActive(true);
    }

    public void ShowQuest()
    {
        ParentPanel.GetComponent<RectTransform>().DOLocalMoveX(showPosX, 1f);

        showButton.SetActive(false);
        hideButton.SetActive(true);
        alertIndicator.SetActive(false);
    }

    public void HideQuest()
    {
        ParentPanel.GetComponent<RectTransform>().DOLocalMoveX(hidePosX, 1f);

        showButton.SetActive(true);
        hideButton.SetActive(false);
        alertIndicator.SetActive(false);
    }

    public void ShowMainQuest()
    {
        sideQuestPanel.SetActive(false);
        MainQuestParentPanel.SetActive(true);
        MainQuestButtonAlpha.color = new Color(0, 0, 0, 0);
        SideQuestButtonAlpha.color = new Color(0, 0, 0, 0.8f);
    }

    public void ShowSideQuest()
    {
        sideQuestPanel.SetActive(true);
        MainQuestParentPanel.SetActive(false);
        MainQuestButtonAlpha.color = new Color(0, 0, 0, 0.8f);
        SideQuestButtonAlpha.color = new Color(0, 0, 0, 0);
    }
}
