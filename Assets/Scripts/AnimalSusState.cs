﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalSusState : StateMachineBehaviour
{
    GameObject unit;
    NavMeshAgent agent;
    SpecialAI cAi;
    AnimalAI ai;

    GameObject target;
    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        unit = animator.gameObject;
        cAi = unit.GetComponent<SpecialAI>();
        agent = unit.GetComponent<NavMeshAgent>();
        ai = unit.GetComponent<AnimalAI>();
        agent.isStopped = true;
        agent.ResetPath();
        cAi.StartDetectionTimer(true);
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (cAi.currentDetectionTime == cAi.maxDetectionTime)
        {
            //Run away
            animator.SetBool("Sus", false);
            animator.SetBool("Flee", true);
        }
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.isStopped = false;
        cAi.StartDetectionTimer(false);
    }
}
