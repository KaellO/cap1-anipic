﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Requirement : ScriptableObject
{
    [TextArea(5, 10)]
    public string Description;
}
