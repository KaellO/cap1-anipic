﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Main Requirement", menuName = "Main Requirement Data")]
public class MainRequirement : Requirement
{
    [Header("Main Requirement")]
    public string RequiredSubject;
}
