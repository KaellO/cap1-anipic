﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalFleeState : StateMachineBehaviour
{
    GameObject unit;
    NavMeshAgent agent;
    SpecialAI cAi;
    AnimalAI ai;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        unit = animator.gameObject;
        cAi = unit.GetComponent<SpecialAI>();
        agent = unit.GetComponent<NavMeshAgent>();
        ai = unit.GetComponent<AnimalAI>();
        agent.isStopped = false;
        agent.speed *= 5;

        cAi.StartFleeTimer();
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        cAi.DoSpecialAction();
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.speed /= 5;
    }
}
