﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DialogueTrigger : MonoBehaviour
{
    public GameObject loadingScreen;
    public Slider loadBar;
    public DialogueManager dManager;
    Dialogue currentDialogue;
    public Dialogue[] AllDialogues;
    public GameObject tempEnd;
    public GameObject folderImage;
    public FadeTransitionUI fade;
    public Sprite[] FileImages;
    bool loadingScene = false;
    private void Start()
    {
        currentDialogue = AllDialogues[TempSavedData.CurrentLevel];
        if (TempSavedData.CurrentLevel == AllDialogues.Length - 1)
            dManager.OnDialogueEnd += GameEnding;
        else
            dManager.OnDialogueEnd += DialogueEnd;

        StartDialogue();
    }

    private void OnDisable()
    {
        dManager.OnDialogueEnd -= DialogueEnd;
    }
    public void StartDialogue()
    {
        dManager.StartDialogue(currentDialogue);
    }

    public void DialogueEnd()
    {
        fade.BlockRayCast(true);
        fade.Fade(1f, 1f, EnableImage);
    }
    void EnableImage()
    {
        folderImage.GetComponent<Image>().sprite = FileImages[TempSavedData.CurrentLevel];
        folderImage.SetActive(true);
    }

    public void AfterFileImage()
    {
        fade.Fade(1f, 1f, LoadGame);
    }

    public void LoadGame()
    {
        if (!loadingScene)
        {
            StartCoroutine(LoadAsync("GameScene"));
            loadingScene = true;
        }
    }

    public void GameEnding()
    {
        if (!loadingScene)
        {
            StartCoroutine(LoadAsync("CutScene"));
            loadingScene = true;
        }
    }

    IEnumerator LoadAsync(string sceneName)
    {
        loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadBar.value = progress;
            yield return null;
        }
    }
}
