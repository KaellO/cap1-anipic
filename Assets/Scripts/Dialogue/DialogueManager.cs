﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;

public class DialogueManager : MonoBehaviour
{
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI DialogueText;
    public GameObject DialoguePanel;
    public Image Portrait;

    Queue<string> sentences = new Queue<string>();
    Queue<Sprite> portraits = new Queue<Sprite>();

    public Action OnDialogueEnd; 

    bool sentenceDone = true;
    string currentSentence;

    public int currentIndex = 0;
    public bool DialogueChanged = false;
    public void StartDialogue(Dialogue dialogue)
    {
        sentences.Clear();
        portraits.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        foreach (Sprite portrait in dialogue.portrait)
        {
            portraits.Enqueue(portrait);
        }

        DisplayNextSentence();
    }
    public void DisplayNextSentence()
    {
        if (!sentenceDone)
        {
            StopAllCoroutines();
            DialogueText.text = currentSentence;
            sentenceDone = true;
            DialogueChanged = false;
            return;
        }

        if (sentences.Count == 0)
        {
            DialogueChanged = false;
            EndDialogue();
            return;
        }
        DialogueChanged = true;
        currentSentence = sentences.Dequeue();
        currentIndex++;
        StopAllCoroutines();
        StartCoroutine(TypeSentence(currentSentence));
        DisplayPortrait();
    }

    public void DisplayPortrait()
    {
        if (portraits.Count == 0)
        {
            Portrait.enabled = false;
            return;
        }

        Sprite p = portraits.Dequeue();

        if (p != null)
        {
            Portrait.enabled = true;
            Portrait.sprite = p;
        }
        else
            Portrait.enabled = false;
    }

    IEnumerator TypeSentence(string sentence)
    {
        sentenceDone = false;
        DialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            DialogueText.text += letter;
            yield return new WaitForSeconds(0.02f);
        }
        sentenceDone = true;
    }

    void EndDialogue()
    {
        OnDialogueEnd?.Invoke();
    }
}
