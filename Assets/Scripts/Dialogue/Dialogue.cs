﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue Data")]
public class Dialogue : ScriptableObject
{
    public string speaker;

    [TextArea(3, 10)]
    public string[] sentences;
    public Sprite[] portrait;
}
