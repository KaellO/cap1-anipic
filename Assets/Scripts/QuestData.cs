﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Quest Data", menuName = "Quest Data")]
public class QuestData : ScriptableObject
{
    public enum QuestType
    {
        MainQuest,
        SideQuest
    }

    public QuestType Type;
    public MainRequirement MainRequirement;

    public SideRequirement[] SideRequirements;
}
