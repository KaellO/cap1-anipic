﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.IO;
using UnityEngine.SceneManagement;
public class PhotoManager : MonoBehaviour
{
    public List<GameObject> AllPhotos = new List<GameObject>();
    public static Action<bool> AllPhotosSelected;
    public static Action<Photo, bool> SetPhotoObject;
    public static PhotoManager instance;

    public List<Photo> photosSelected;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(instance);
    }
    /*
     * Fix this dogshit
     *      1. why the fuck are there 2 photos selected lists
     *      2. only allow selection of 1 photo that pertains to 1 quest.
     *      3. all main quests selected = allow turning in
     *      4. side quests should be optional.
     */
    public bool SelectPhoto(Photo photo)
    {
        if (photo.Rating <= 0)
            return false;

        //foreach (Photo p in photosSelected)
        //{
        //    if (photo.AttachedQuest == p.AttachedQuest && !photo.Select)
        //    {
        //        return false;
        //    }
        //}

        photo.Select = !photo.Select;

        if (photo.Select)
            photosSelected.Add(photo);
        else if (photosSelected.Contains(photo))
            photosSelected.Remove(photo);

        List<GameObject> mainPhotos = AllPhotos.Where(p => p.GetComponent<Photo>().Select == true &&
            p.GetComponent<Photo>().AttachedQuest.QuestData.Type == QuestData.QuestType.MainQuest).ToList();

        List<GameObject> sideQuest = AllPhotos.Where(p => p.GetComponent<Photo>().Select == true &&
            p.GetComponent<Photo>().AttachedQuest.QuestData.Type == QuestData.QuestType.SideQuest).ToList();

        if (mainPhotos.Count() == QuestManager.instance.mainQuests.Count())
        {
            AllPhotosSelected?.Invoke(true);
        }
        else
            AllPhotosSelected?.Invoke(false);

        foreach(Photo p in photosSelected)
        {
            Debug.Log(p.FilePath);
        }

        //disable selecting other photos wiht the same quest
        if (photo.Select)
        {
            foreach (GameObject p in AllPhotos)
            {
                if (photo.AttachedQuest == p.GetComponent<Photo>().AttachedQuest && !p.GetComponent<Photo>().Select)
                {
                    SetPhotoObject(p.GetComponent<Photo>(), false);
                }
            }
        }
        else
            foreach (GameObject p in AllPhotos)
            {
                if (photo.AttachedQuest == p.GetComponent<Photo>().AttachedQuest && !p.GetComponent<Photo>().Select)
                {
                    SetPhotoObject(p.GetComponent<Photo>(), true);
                }
            }

        //add to list

        return true;
    }

    public void ClearSelectedPhotos()
    {
        photosSelected.Clear();
    }

    private void OnApplicationQuit()
    {
        /*
         * Put Saved Photos in a different file if saving photos is added in the future.
         * Call this code when the next level is played also via GameManager.
         */

        ClearFolder(Application.dataPath + "/PhotosTaken(Temp)/");

        //Clears the saved folder use only for development
        ClearFolder(Application.dataPath + "/PhotosSelected/");
    }

    void ClearFolder(string filePath)
    {
        DeleteFilesInFolder(filePath);
    }

    void SavePhotoFile(List<Photo> photos)
    {
        //string[] files = Directory.GetFiles(Application.dataPath + "/PhotosTaken(Temp)/");

        //foreach (string file in files)
        //{
        //    foreach (Photo photo in photos)
        //    {
        //        if (file == photo.FilePath)
        //        {
        //            File.Move(file, Application.dataPath + "/PhotosSelected/" + photo.FileName);
        //            Debug.Log("Move");
        //        }
        //    }
        //}

        foreach (Photo photo in photos)
        {
            File.Move(photo.FilePath, Application.dataPath + "/PhotosSaved/" + photo.FileName);
        }
    }
    bool photosSubmited = false;
    public void SubmitPhotos()
    {
        if (!photosSubmited)
        {
            TempSavedData.CurrentLevel++;
            SavePhotoData();
            photosSubmited = true;
        }
        //ClearFolder(Application.dataPath + "/PhotosTaken(Temp)/");
    }

    float[] FollowerMult = { 1, 1.5f, 2, 3, 4 };

    void SavePhotoData()
    {
        //calculate followers gained
        float maxPoints = 0;
        float currentPoints = 0;
        float addedFollowers = 0;
        foreach (Photo photo in photosSelected)
        {
            maxPoints += 3;
            currentPoints += photo.Rating;
        }


        float mult = UnityEngine.Random.Range(90, 130);
        addedFollowers = Mathf.Ceil((currentPoints / maxPoints) * mult)
            * FollowerMult[TempSavedData.CurrentLevel];

        //Very tentative formula for follower count
        addedFollowers *= photosSelected.Count;
        addedFollowers = Mathf.Ceil(addedFollowers);
        //Temporarily save to static Script
        TempSavedData.photoData.AddRange(photosSelected);
        TempSavedData.addedFollowers = addedFollowers;

        //Save to JSON
        //SaveObject saveObject = new SaveObject
        //{
        //    addedFollowers = addedFollowers
        //};

        //string json = JsonUtility.ToJson(saveObject);
        //File.WriteAllText(Application.dataPath + "/SaveFiles/" + "/save.txt", json);
    }

    void DeleteFilesInFolder(string filePath)
    {
        if (Directory.Exists(filePath)) 
        { 
            Directory.Delete(filePath, true); 
        }

        Directory.CreateDirectory(filePath);
    }
}

public class SaveObject
{
    public float addedFollowers;
    public float totalFollowers;
}
