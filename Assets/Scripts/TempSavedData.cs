﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TempSavedData
{
    public static List<Photo> photoData = new List<Photo>();
    public static float addedFollowers = 0;
    public static float totalFollowers;
    public static int CurrentLevel = 0;

    //Compendium
    public static bool UnlockedTamaraw = false;
    public static bool UnlockedTurtle = false;
    public static bool UnlockedCroc = false;
    public static bool UnlockedMouseDeer = false;

    //Settings
    public static float MouseSensitivity = 50;
    public static bool UseMotionControlls = true;
}
