﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroCam : MonoBehaviour
{
    Camera mainCamera;
    bool gyroEnabled = false;
    private Gyroscope gyro;
    public bool UseGyro;
    public GameObject playerBody;
    CameraMechanic camMech;

    Quaternion initialRotation;
    Quaternion oldPlayerRot;
    Quaternion offset;

    private void Awake()
    {
        camMech = GetComponentInParent<CameraMechanic>();
    }

    private void OnEnable()
    {
        initialRotation = transform.localRotation;

        oldPlayerRot = playerBody.transform.rotation;   
        offset = transform.rotation * Quaternion.Inverse(GyroToUnity(Input.gyro.attitude));
        gyroEnabled = EnableGyro();
    }
    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope && UseGyro)
        {
            gyro = Input.gyro;
            gyro.enabled = true;

            //playerBody.transform.rotation = Quaternion.Euler(90, 90, 0);
            return true;
        }
        return false;
    }
    // Update is called once per frame
    void Update()
    {
        if (gyroEnabled)
        {
            transform.rotation = offset * GyroToUnity(gyro.attitude);

            camMech.CheckRayDist(transform.position, transform.forward);
        }
    }
    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }
    private void OnDisable()
    {
        playerBody.transform.rotation = oldPlayerRot;
    }

    public void EnableGyro(bool b)
    {
        UseGyro = b;
        TempSavedData.UseMotionControlls = b;
    }
}
