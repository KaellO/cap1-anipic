﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectionUI : MonoBehaviour
{
    public Image detectionIndicator;
    public Image detectionProgression;
    public GameObject container;
    SpecialAI ai;
    CameraMechanic cam;

    bool currentActiveStatus = false;
    private void Awake()
    {
        ai = GetComponentInParent<SpecialAI>();
        cam = GameObject.FindObjectOfType<CameraMechanic>();
        cam.OnRenderPhoto += DisableForPhoto;
        cam.OnPhotoTaken += EnableForPhoto;
        ai.OnUpdateDetectionProgressUI += UpdateDetectionUI;
        ai.OnFlee += Flee;
        ai.OnChangeDetectionColor += ChangeDetectionColor;
        ai.OnToggleDetectionTimer += ToggleDetection;
        ToggleDetection(false);
    }

    void UpdateDetectionUI(float rate)
    {
        detectionProgression.fillAmount = rate;
    }

    void Flee()
    {
        detectionProgression.enabled = false;
    }

    void ToggleDetection(bool c)
    {
        container.SetActive(c);
        currentActiveStatus = c;
    }

    void DisableForPhoto()
    {
        container.SetActive(false);
    }

    void EnableForPhoto(GameObject g, Texture2D tex)
    {
        if (currentActiveStatus)
            container.SetActive(true);
    }

    void ChangeDetectionColor(Color color)
    {
        detectionIndicator.color = color;
        if(color == Color.yellow)
            detectionProgression.enabled = true;
    }
}
