﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Canvas uiCanvas;
    // Start is called before the first frame update
    public Joystick joystick;
    //public FixedTouchField fixedTouchField;

    Vector2 joystickInput;

    private Animator animator;
    public Movement movementComp;

    bool freecam = false;
    void Start()
    {
        animator = GetComponent<Animator>();
        movementComp = GetComponent<Movement>();
    }

    private void Update()
    {
        joystickInput.x = joystick.Horizontal;
        joystickInput.y = joystick.Vertical;
         
        Vector3 move = transform.right * joystickInput.x + transform.forward * joystickInput.y;

        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //    GetComponent<CharacterController>().enabled = false;
        //    GetComponent<CapsuleCollider>().enabled = false;
        //    movementComp.enabled = false;
  
        //    freecam = true;
        //}
        //if (!freecam)
            movementComp.Move(move);
        //else
        //    transform.position += move * 20 * Time.deltaTime;
    }
}
