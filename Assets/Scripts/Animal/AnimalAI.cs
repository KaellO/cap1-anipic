﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalAI : MonoBehaviour
{
    NavMeshAgent agent;
    Animator animator;
    public AnimalTriviaData triviaData;

    //Movement AI Variables
    Vector3 originalPosition;

    [SerializeField] float wanderRadius = 4f; //So that the animal wont wander too far from its original spawn point
    [SerializeField] float minIdleTime = 3f;
    [SerializeField] float maxIdleTime = 5f;
    [SerializeField] float animatationOffset = 1;
    Vector3 gizmoPosition;
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    void Start()
    {
        originalPosition = transform.position;
    }

    private void Update()
    {
        animator.SetFloat("Speed", agent.velocity.magnitude * animatationOffset);
    }

    public void StartWanderTimer()
    {
        StartCoroutine(WanderTimer());
    }
    IEnumerator WanderTimer()
    {
        float time = Random.Range(minIdleTime, maxIdleTime);
        yield return new WaitForSeconds(time);

        agent.isStopped = false;
        MoveTo(GetRandomPointInNavMesh());

        //animator.SetBool("Idle", false);
        //animator.SetBool("Wander", true);
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Vector3 newPosition = transform.position + gizmoPosition;
        Gizmos.DrawWireSphere(newPosition, wanderRadius);
    }

    public Vector3 GetRandomPointInNavMesh()
    {
        Vector3 randomDirection = Random.insideUnitSphere * wanderRadius;

        //origin of the sphere
        randomDirection += originalPosition;

        NavMeshHit hit;
        if (NavMesh.SamplePosition(randomDirection, out hit, wanderRadius, agent.areaMask))
        {
            return hit.position;
        }
        else
        {
            //Debug.LogError(agent.name + " " + "SAMPLE POSITION ERROR");
            return transform.position;
        }
    }

    public void MoveTo(Vector3 location)
    {
        agent.ResetPath();

        Vector3 endPoint = new Vector3(location.x, transform.position.y, location.z);
        NavMeshPath path = new NavMeshPath();

        agent.CalculatePath(endPoint, path);

        if (path.status != NavMeshPathStatus.PathInvalid)
            agent.SetDestination(endPoint);
    }

    public void MoveInDir(Vector3 dir)
    {
        transform.Rotate(dir);
    }
}
