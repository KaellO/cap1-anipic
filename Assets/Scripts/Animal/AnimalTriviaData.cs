﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Animal Trivia", menuName = "Animal Trivia")]
public class AnimalTriviaData : ScriptableObject
{
    public string ScientificName;
    [TextArea(5, 10)]
    public string Description;
}
