﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;

    public LayerMask TargetMask;
    public LayerMask ObstacleMask;

    public Transform visiblePlayer;

    public Action<GameObject> OnPlayerDetected;
    public Action OnPlayerExited;

    bool detectedPlayer = false;

    void Start()
    {
        StartCoroutine(FindTargetsWithDelay(0.2f));
    }

    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);

            FindVisibleTargets();
        }
    }

    void FindVisibleTargets()
    {
        visiblePlayer = null;

        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, TargetMask);

        //check if target is in radius
        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform target = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;

            //check if target is in sight range
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float distToTarget = Vector3.Distance(transform.position, target.position);

                //Check for obstacles
                if (!Physics.Raycast(transform.position, dirToTarget, distToTarget, ObstacleMask))
                {
                    visiblePlayer = target;
                    detectedPlayer = true;
                    OnPlayerDetected?.Invoke(target.gameObject);
                }
                //If behind obstacle
                else
                {
                    OnPlayerExited?.Invoke();
                    detectedPlayer = false;
                }
            }
        }
        
        //If out of range
        if (detectedPlayer && targetsInViewRadius.Length == 0)
        {
            OnPlayerExited?.Invoke();
            detectedPlayer = false;
        }
    }

    public Vector3 DirFromAngle(float angleInDeg, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
            angleInDeg += transform.eulerAngles.y;

        return new Vector3(Mathf.Sin(angleInDeg * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDeg * Mathf.Deg2Rad));

    }
}
