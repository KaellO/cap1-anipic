﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;

public class SpecialAI : MonoBehaviour
{
    protected NavMeshAgent agent;
    protected Animator animator;
    protected FieldOfView fov;
    protected AnimalAI baseAI;

    public Action OnSuspicious;
    public Action<float> OnUpdateDetectionProgressUI;
    public Action OnFlee;
    public Action<Color> OnChangeDetectionColor;
    public Action<bool> OnToggleDetectionTimer;

    protected Coroutine detectionTimerCoroutine;
    protected GameObject target;
    protected bool timerDone;

    [Header("Detection Stats")]
    public float maxDetectionTime;
    [HideInInspector] public float currentDetectionTime = 0;
    [SerializeField] float timerLength = 3;

    protected void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        fov = GetComponent<FieldOfView>();
        baseAI = GetComponent<AnimalAI>();
    }

    protected virtual void Start()
    {
        fov.OnPlayerDetected += Suspicious;
        fov.OnPlayerExited += PlayerOutOfRange;
    }

    protected void Suspicious(GameObject player)
    {
        animator.SetBool("Sus", true);
        target = player;
        OnToggleDetectionTimer?.Invoke(true);
    }

    protected void PlayerOutOfRange()
    {
        animator.SetBool("Sus", false);
    }

    public void StartDetectionTimer(bool start)
    {
        float endTime = start ? maxDetectionTime : 0;
        StopAllCoroutines();
        detectionTimerCoroutine = StartCoroutine(DetectionTimer(endTime));
    }

    public void StartFleeTimer()
    {
        StartCoroutine(FleeTimer());
    }
    bool vibrated = false;

    public virtual void DoSpecialAction()
    {
        OnFlee?.Invoke();
        OnChangeDetectionColor?.Invoke(Color.red);
        if(!vibrated)
        {
            Handheld.Vibrate();
            vibrated = true;
        }
    }

    protected IEnumerator FleeTimer()
    {
        float currentTime = 0;
        while (currentTime < timerLength)
        {
            currentTime = Mathf.MoveTowards(currentTime, timerLength, Time.deltaTime);
            yield return null;
        }

        timerDone = true;
    }

    protected IEnumerator DetectionTimer(float endTime)
    {
        do
        {
            currentDetectionTime = Mathf.MoveTowards(currentDetectionTime, endTime, Time.deltaTime);

            OnUpdateDetectionProgressUI?.Invoke(currentDetectionTime / maxDetectionTime);

            yield return null;
        } while (currentDetectionTime > 0 && currentDetectionTime < maxDetectionTime);

        Mathf.Clamp(currentDetectionTime, 0, maxDetectionTime);

        if (currentDetectionTime == 0)
        {
            OnToggleDetectionTimer?.Invoke(false);
        }
    }

    protected virtual void ResetAnimal()
    {
        OnChangeDetectionColor?.Invoke(Color.yellow);
        vibrated = false;
    }
}
