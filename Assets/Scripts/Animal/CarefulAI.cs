﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;

public class CarefulAI : SpecialAI
{
    public List<Transform> SpawnAreas;
    float maxFleeTime = 3;
    public GameObject Player;

    public override void DoSpecialAction()
    {
        base.DoSpecialAction();

        Vector3 fleeDirection = transform.position - target.transform.position;

        Vector3 runTo = transform.position + fleeDirection;
        agent.SetDestination(runTo);

        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(fleeDireciton), 1.0f * Time.deltaTime);
        //transform.Translate(0, 0, Time.deltaTime * 2.0f);

        if (timerDone)
        {
            ResetAnimal();
            animator.SetBool("Flee", false);
            animator.SetBool("Idle", true);
            timerDone = false;
        }

        return;

        if (target)
        {
            Vector3 FleeDir = target.transform.position - transform.position;
            baseAI.MoveTo(target.transform.position + target.transform.forward * (FleeDir.magnitude * 10));   
        }
    }

    protected override void ResetAnimal()
    {
        OnChangeDetectionColor?.Invoke(Color.yellow);

        /*
         * Chooses randomly from the furthest spawn areas 
        */
        List<Transform> transforms = SpawnAreas.OrderByDescending(area => Vector3.Distance(area.position, transform.position)).ToList();

        if (SpawnAreas.Count > 0)
        {
            agent.Warp(transforms[0].position);
            Debug.Log(transforms[0].name);
        }
        else
            Debug.LogWarning("No Spawn Areas");

    }
}
