﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalWanderState : StateMachineBehaviour
{
    GameObject unit;
    NavMeshAgent agent;
    AnimalAI ai;
    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        unit = animator.gameObject;
        agent = unit.GetComponent<NavMeshAgent>();
        ai = unit.GetComponent<AnimalAI>();
        //agent.isStopped = false;

        //ai.MoveTo(ai.GetRandomPointInNavMesh());
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0)
        //{
        //    animator.SetBool("Wander", false);
        //    animator.SetBool("Idle", true);
        //}
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.SetBool("Wander", false);
    }
}
