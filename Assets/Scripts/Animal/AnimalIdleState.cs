﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalIdleState : StateMachineBehaviour
{
    GameObject unit;
    NavMeshAgent agent;
    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("Idle", true);
        unit = animator.gameObject;
        agent = unit.GetComponent<NavMeshAgent>();

        agent.ResetPath();
        agent.isStopped = true;

        unit.GetComponent<AnimalAI>().StartWanderTimer();
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //if (agent.hasPath)
        //{
        //    animator.SetBool("Idle", false);
        //}
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.SetBool("Idle", false);
    }


}
