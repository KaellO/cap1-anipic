﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;
using UnityEngine.AI;

public class PursueAI : MonoBehaviour
{
    public float SphereRad;
    //public override void DoSpecialAction()
    //{
    //    base.DoSpecialAction();

    //    //Maybe none for now 
    //    //if(target)
    //    //    agent.SetDestination(target.transform.position);
    //}



    private void Update()
    {
        Collider[] col = Physics.OverlapSphere(transform.position, SphereRad, LayerMask.GetMask("Player"));
        if (col.Count() != 0)
        {
            col[0].GetComponent<PlayerAction>().Crocodile();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, SphereRad);
    }
}
