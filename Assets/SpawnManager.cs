﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [System.Serializable]
    public class AnimalList
    {
        public List<GameObject> AnimalPrefabs = new List<GameObject>();
    }

    public List<AnimalList> AnimalsToSpawn;

    void Awake()
    {
        List<GameObject> animalsToSpawn = AnimalsToSpawn[TempSavedData.CurrentLevel].AnimalPrefabs;

        foreach (GameObject animal in animalsToSpawn)
        {
            animal.SetActive(true);
        }
    }
}
