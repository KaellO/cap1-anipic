﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompendiumThumbUI : MonoBehaviour
{
    [Header("UI Elements")]
    public GameObject thumbnail;
    public GameObject SubjectName;
    public Button button;
    public Sprite UnlockedSprite;
    public IndivImageUI imageUI;
    public ModelViewerUI modelViewerUI;
    [Header("Data")]
    public AnimalTriviaData AnimalTrivia;
    public string AnimalName;
    [Header("3DViewer")]
    public GameObject AnimalRenderer;
    public GameObject AnimalMesh;

    public bool Unlocked = false;

    public void Unlock()
    {
        thumbnail.GetComponent<Image>().sprite = UnlockedSprite;
        button.onClick.AddListener(SetUp3DView);
        button.interactable = true;
        Unlocked = true;
    }

    public void SetUp3DView()
    {
        imageUI.Name.text = AnimalName;
        imageUI.ScientificName.text = AnimalTrivia.ScientificName;
        imageUI.Description.text = AnimalTrivia.Description;
        modelViewerUI.AnimalMesh = AnimalMesh;
        modelViewerUI.AnimalRenderer = AnimalRenderer;

        AnimalRenderer.SetActive(true);
        AnimalMesh.SetActive(true);
    }
}
