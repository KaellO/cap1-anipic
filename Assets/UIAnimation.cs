﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class UIAnimation : MonoBehaviour
{
    RectTransform rectTransform;

    public Vector3 endPosition;
    public float duration;
    public bool pingpong;
    public UnityEvent OnTweenComplete;
    Tween tween;
    Vector3 oldTransform;
    // Start is called before the first frame update
    void Start()
    {
        oldTransform = transform.position;
        rectTransform = GetComponent<RectTransform>();

        if (pingpong)
        {
            PingPong();
        }

        Debug.Log(name + " " + transform.position);
    }

    public void StartTween()
    {
        tween = rectTransform.DOMove(endPosition, duration).OnComplete(() => OnTweenComplete?.Invoke());
    }

    public void PingPong()
    {
        rectTransform.DOMove(endPosition, duration).OnComplete(() =>
        rectTransform.DOMove(oldTransform, duration).OnComplete(PingPong));
    }
    public void Reset()
    {
        transform.position = oldTransform;
    }
}
