﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepTrigger : MonoBehaviour
{
    public string footstep;
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<Footsteps>()?.ChangeFootstep(footstep);
    }
}
