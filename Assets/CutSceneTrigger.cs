﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class CutSceneTrigger : MonoBehaviour
{
    public GameObject loadingScreen;
    public Slider loadBar;
    public DialogueManager dManager;
    Dialogue currentDialogue;
    public Dialogue[] AllDialogues;
    public GameObject tempEnd;
    public FadeTransitionUI fade;
    public SlideShowUI slideShow;

    [Header("Cutscene switch")]
    [SerializeField] int[] IntroIndices;
    [SerializeField] int[] EndingIndices;
    [SerializeField] AudioClip IntroClip;
    [SerializeField] AudioClip EndClip;
    [SerializeField] GameObject EndingPanel;
    int[] IndicesUsed;

    bool loadingScene = false;
    bool gameEnd = false;
    private void Start()
    {
        if (TempSavedData.CurrentLevel == 0)
        {
            IndicesUsed = IntroIndices;
            currentDialogue = AllDialogues[TempSavedData.CurrentLevel];
            GetComponent<AudioSource>().clip = IntroClip;
            GetComponent<AudioSource>().Play();
        }
        else
        {
            IndicesUsed = EndingIndices;
            currentDialogue = AllDialogues[1];
            GetComponent<AudioSource>().clip = EndClip;
            GetComponent<AudioSource>().Play();
        }

        dManager.OnDialogueEnd += DialogueEnd;

        StartDialogue();
    }

    private void OnDisable()
    {
        dManager.OnDialogueEnd -= DialogueEnd;
    }
    public void StartDialogue()
    {
        dManager.StartDialogue(currentDialogue);
    }

    public void NextDialogueSequenceWrapper()
    {
        dManager.DisplayNextSentence();

        if (!dManager.DialogueChanged)
            return;

        foreach (int i in IndicesUsed)
        {
            if(i == dManager.currentIndex)
            {
                Debug.Log(dManager.currentIndex);
                slideShow.DisplayNextSlide();
                return;
            }
        }
    }

    public void DialogueEnd()
    {
        if (TempSavedData.CurrentLevel == 0)
        {
            fade.BlockRayCast(true);
            fade.Fade(1f, 1f, LoadGame);
        }
        else
        {
            if (!gameEnd)
                EndGame();
            else if (!loadingScene)
                LoadMenu();
        }
    }
    public void EndGame()
    {
        slideShow.Slides[IndicesUsed.Length-1].GetComponent<CanvasGroup>()
        .DOFade(0f, 0.5f);

        EndingPanel.SetActive(true);

        EndingPanel.GetComponent<CanvasGroup>().DOFade(1f, 0.5f).OnComplete(() => StartCoroutine(EndGameBuffer()));
    }

    IEnumerator EndGameBuffer()
    {
        yield return new WaitForSeconds(5f);
        gameEnd = true;
    }

    public void LoadGame()
    {
        if (!loadingScene)
        {
            StartCoroutine(LoadAsync("DialogueScene"));
            loadingScene = true;
        }
    }

    public void LoadMenu()
    {
        TempSavedData.addedFollowers = 0;
        TempSavedData.totalFollowers = 0;
        TempSavedData.CurrentLevel = 0;

        //Compendium
        TempSavedData.UnlockedTamaraw = false;
        TempSavedData.UnlockedTurtle = false;
        TempSavedData.UnlockedCroc = false;
        TempSavedData.UnlockedMouseDeer = false;

        if (!loadingScene)
        {
            StartCoroutine(LoadAsync("MenuScene"));
            loadingScene = true;
        }
    }

    IEnumerator LoadAsync(string sceneName)
    {
        loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadBar.value = progress;
            yield return null;
        }
    }
}
