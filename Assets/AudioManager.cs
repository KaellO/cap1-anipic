﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Audio;
public enum SoundGroupName
{
    Camera,
    Music,
    Ambient,
    FootSteps
}
public class AudioManager : MonoBehaviour
{
    [System.Serializable]
    public class SoundGroup
    {
        public SoundGroupName name;
        public Sound[] sounds;

        public Sound GetSound(string name)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound " + name + " does not exist");
                return null;
            }
            return s;
        }

        public Sound GetSound(AudioClip name)
        {
            Sound s = Array.Find(sounds, sound => sound.clip == name);
            if (s == null)
            {
                Debug.LogWarning("Sound " + name + " does not exist");
                return null;
            }
            return s;
        }

        public string GetSoundName(AudioClip clip)
        {
            Sound s = Array.Find(sounds, sound => sound.clip == clip);
            if (s == null)
            {
                Debug.LogWarning("Name " + name + " not found");
                return null;
            }
            return s.name;
        }
    }

    public SoundGroup[] SoundGroups;
    List<Sound> soundsPlaying = new List<Sound>();

    private void Awake()
    {
        foreach (SoundGroup group in SoundGroups)
        {
            foreach (Sound s in group.sounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;

                s.source.volume = s.volume;
                s.source.pitch = s.pitch;
                s.source.loop = s.loop;
            }    
        }
    }

    public void PlaySound(SoundGroupName SoundGroup, string AudioName)
    {
        SoundGroup group = Array.Find(SoundGroups, sg => sg.name == SoundGroup);
        if (group == null)
        {
            Debug.LogWarning("SoundGroup " + AudioName + " does not exist");
            return;
        }

        Sound soundToPlay = group.GetSound(AudioName);

        if(soundToPlay.loop)
            soundsPlaying.Add(soundToPlay);

        soundToPlay.source.Play();
    }

    public void PlaySound(SoundGroupName SoundGroup, AudioClip AudioClip)
    {
        SoundGroup group = Array.Find(SoundGroups, sg => sg.name == SoundGroup);
        if (group == null)
        {
            Debug.LogWarning("SoundGroup " + AudioClip + " does not exist");
            return;
        }

        Sound soundToPlay = group.GetSound(AudioClip);

        if (soundToPlay.loop)
            soundsPlaying.Add(soundToPlay);

        soundToPlay.source.Play();
    }

    public void StopSound(string name)
    {
        Sound sound = soundsPlaying.Find(s => s.name == name);

        if (sound == null)
        {
            Debug.LogWarning("Sound " + name + " is not playing");
            return;
        }
        soundsPlaying.Remove(sound);
        sound.source.Stop();
    }
}
