﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class FadeTransitionUI : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvas;
    private Tween fadeTween;

    public void Fade(float endValue, float duration, TweenCallback OnEnd)
    {
        if (fadeTween != null)
        {
            fadeTween.Kill(false);
        }

        fadeTween = canvas.DOFade(endValue, duration);
        fadeTween.onComplete += OnEnd;
    }

    IEnumerator BackupTimer(float endValue, float time, TweenCallback OnEnd)
    {
        yield return new WaitForSeconds(time);
        fadeTween.Kill(false);
        fadeTween = canvas.DOFade(endValue, 0f);
        fadeTween.onComplete += OnEnd;
    }

    public void BlockRayCast(bool c)
    {
        canvas.interactable = c;
        canvas.blocksRaycasts = c;
    }
}
