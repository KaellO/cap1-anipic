﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class FileImageUI : MonoBehaviour
{
    public FadeTransitionUI FadeTransition;
    public GameObject GoToNextLevel;
    public UnityEvent OnGoNext;
    private void OnEnable()
    {
        FadeTransition.Fade(0f, 1f, AfterFadeIn);
    }

    void AfterFadeIn()
    {
        StartCoroutine(initialTimer());
    }
    IEnumerator initialTimer()
    {
        yield return new WaitForSeconds(3);
        GoToNextLevel.SetActive(true);
    }

    private void OnDisable()
    {
        FadeTransition.Fade(1f, 1f, () => OnGoNext?.Invoke());
    }
}
